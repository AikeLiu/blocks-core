{-# LANGUAGE LambdaCase #-}

module Blocks.Memoize where

import           Control.Concurrent.STM (atomically, modifyTVar, newEmptyTMVar,
                                         newTVarIO, putTMVar, readTMVar,
                                         readTVar)
import qualified Data.Map.Strict        as Map

-- Memoize an IO computation by storing a Map of MVar's
-- containing results.  Ensures that the function f is run at most
-- once for each argument.
memoize :: (Ord t) => (t -> IO a) -> IO (t -> IO a)
memoize f = do
  cache <- newTVarIO Map.empty
  let lookupCache a =
        Map.lookup a <$> readTVar cache >>= \case
          -- Computation never run before, make a new TMVar for result
          Nothing -> do
            v <- newEmptyTMVar
            modifyTVar cache (Map.insert a v)
            return (Left v)
          -- Computation already memoized, return a TMVar for the result
          Just v  -> return (Right v)
  return $ \a ->
    atomically (lookupCache a) >>= \case
      -- Computation never run before, run it now
      Left v -> do
        x <- f a
        atomically (putTMVar v x)
        return x
      -- Computation already run, read TMVar for result. Will block
      -- until the computation is complete.
      Right v ->
        atomically (readTMVar v)
