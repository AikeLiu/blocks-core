{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric  #-}

module Blocks.Delta where

import           Data.Aeson   (FromJSON, ToJSON)
import           Data.Binary  (Binary)
import           GHC.Generics (Generic)

data Delta
  = Fixed Rational
  | RelativeUnitarity Rational
  deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON)
