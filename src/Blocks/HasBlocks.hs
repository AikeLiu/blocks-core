{-# LANGUAGE ConstraintKinds            #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE DefaultSignatures          #-}
{-# LANGUAGE DeriveAnyClass             #-}
{-# LANGUAGE DeriveGeneric              #-}
{-# LANGUAGE DerivingStrategies         #-}
{-# LANGUAGE DuplicateRecordFields      #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE InstanceSigs               #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE PolyKinds                  #-}
{-# LANGUAGE RankNTypes                 #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TypeApplications           #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE TypeOperators              #-}

module Blocks.HasBlocks where

import           Blocks.Coordinate               (Derivative (..),
                                                  TaylorCoeff (..))
import           Bootstrap.Math.DampedRational   (DampedRational)
import qualified Bootstrap.Math.DampedRational   as DR
import           Bootstrap.Math.FreeVect         (FreeVect, evalFreeVectM)
import qualified Bootstrap.Math.RationalFunction as R
import           Bootstrap.Math.Util             (factorial)
import           Bootstrap.Math.VectorSpace      (Tensor, VectorSpace (..),
                                                  scale, (^/))
import           Control.Applicative             (liftA2)
import           Control.Monad.Zip               (mzipWith)
import qualified Data.Foldable                   as Foldable
import           Data.Functor.Compose            (Compose (..))
import           Data.Functor.Identity           (Identity (..))
import           Data.Kind                       (Type)
import           Data.Matrix.Static              (Matrix)
import           Data.Proxy                      (Proxy (..))
import           Data.Reflection                 (Reifies, reflect, reify)
import           Data.Tagged                     (Tagged, proxy)
import           Data.Vector                     (Vector)
import           GHC.Exts                        (Constraint)
import           Linear.V                        (Dim, V)
import qualified Linear.V                        as L

type HasBlocks b p m a = ( BlockFetchContext b a m
                         , Reifies p (BlockTableParams b)
                         , Applicative m
                         )

-- Really need to define a library module for this
runTagged :: forall a r. a -> (forall (s :: Type) . Reifies s a => Tagged s r) -> r
runTagged a f = reify a (proxy f)

type family BlockFetchContext b a (m :: Type -> Type) :: Constraint

type family BlockTableParams b :: Type

type family BlockBase b a :: Type

class (RealFloat a, Eq a) => IsolatedBlock b deriv a where
  getBlockIsolated
    :: (HasBlocks b p m a, Applicative v, Foldable v)
    => v deriv
    -> b
    -> Compose (Tagged p) m (v a)

-- | If b has a ContinuumBlock instance, we can compute an isolated
-- block by evaluating the DampedRational at 0. It may sometimes be
-- possible to write more efficient versions of getBlockIsolated' by
-- hand.
getBlockIsolatedDefault
  :: (ContinuumBlock b deriv a, Reifies (BlockBase b a) a, HasBlocks b p m a, Applicative v, Foldable v)
  => v deriv
  -> b
  -> Compose (Tagged p) m (v a)
getBlockIsolatedDefault dv b = DR.evalCancelPoleZeros 0 <$> getBlockContinuum dv b

class IsolatedBlock b deriv a => ContinuumBlock b deriv a where
  getBlockContinuum
    :: (HasBlocks b p m a, Applicative v, Foldable v)
    => v deriv
    -> b
    -> Compose (Tagged p) m (DR.DampedRational (BlockBase b a) v a)

type instance BlockFetchContext (Either b1 b2) a m = (BlockFetchContext b1 a m, BlockFetchContext b2 a m)
type instance BlockTableParams (Either b1 b2) = (BlockTableParams b1, BlockTableParams b2)

instance ( IsolatedBlock b1 deriv a
         , IsolatedBlock b2 deriv a
         ) => IsolatedBlock (Either b1 b2) deriv a where
  getBlockIsolated
    :: forall p m v . (HasBlocks (Either b1 b2) p m a, Applicative v, Foldable v)
    => v deriv
    -> (Either b1 b2)
    -> Compose (Tagged p) m (v a)
  getBlockIsolated dv (Left b1) =
    let (p1,_) = reflect @p Proxy
    in Compose $ pure $ runTagged p1 $ getCompose $ getBlockIsolated dv b1
  getBlockIsolated dv (Right b2) =
    let (_,p2) = reflect @p Proxy
    in Compose $ pure $ runTagged p2 $ getCompose $ getBlockIsolated dv b2

-- TODO: This is kind of awkward because it is only correct when
-- BlockBase b1 a ~ BlockBase b2 a. However, we cannot add a
-- constraint to a type instance declaration. It makes it look like
-- BlockBase should probably be a parameter to the ContinuumBlock
-- typeclass (perhaps with FunctionalDepdenencies)
type instance BlockBase (Either b1 b2) a = BlockBase b1 a

instance ( ContinuumBlock b1 deriv a
         , ContinuumBlock b2 deriv a
         , BlockBase b1 a ~ BlockBase b2 a
         ) => ContinuumBlock (Either b1 b2) deriv a where
  getBlockContinuum
    :: forall p m v . (HasBlocks (Either b1 b2) p m a, Applicative v, Foldable v)
    => v deriv
    -> (Either b1 b2)
    -> Compose (Tagged p) m (DR.DampedRational (BlockBase (Either b1 b2) a) v a)
  getBlockContinuum dv (Left b1) =
    let (p1,_) = reflect @p Proxy
    in Compose $ pure $ runTagged p1 $ getCompose $ getBlockContinuum dv b1
  getBlockContinuum dv (Right b2) =
    let (_,p2) = reflect @p Proxy
    in Compose $ pure $ runTagged p2 $ getCompose $ getBlockContinuum dv b2

instance ( IsolatedBlock b deriv a
         , deriv ~ Derivative c
         ) => IsolatedBlock b (TaylorCoeff deriv) a where
  getBlockIsolated derivV b =
    fmap (liftA2 taylorRescale derivV) $
    getBlockIsolated (fmap unTaylorCoeff derivV) b
    where
      taylorRescale t = runIdentity . taylorRescaleV t . Identity

instance ( ContinuumBlock b deriv a
         , deriv ~ Derivative c
         ) => ContinuumBlock b (TaylorCoeff deriv) a where
  getBlockContinuum derivV b =
    fmap (DR.mapNumerator (liftA2 taylorRescaleV derivV)) $
    getBlockContinuum (fmap unTaylorCoeff derivV) b

taylorRescaleV
  :: (VectorSpace v, IsBaseField v a, Fractional a) => TaylorCoeff (Derivative c) -> v a -> v a
taylorRescaleV (TaylorCoeff (Derivative (m,n))) v =
  v ^/ (fromIntegral (factorial m) * fromIntegral (factorial n))

getBlockExprIsolated
  :: (HasBlocks b p m a, VectorSpace v, IsBaseField v a, Applicative v, Foldable v, IsolatedBlock b deriv a)
  => v deriv
  -> Rational
  -> FreeVect (R.RationalFunction Rational, b) a
  -> Compose (Tagged p) m (v a)
getBlockExprIsolated derivVec xVal =
  evalFreeVectM $ \(rationalFn, block) ->
  let r = fromRational (R.eval rationalFn xVal)
  in scale r <$> getBlockIsolated derivVec block

getBlockExprContinuum
  :: (HasBlocks b p m a, Applicative v, Foldable v, ContinuumBlock b deriv a)
  => v deriv
  -> Rational
  -> FreeVect (R.RationalFunction Rational, b) a
  -> Compose (Tagged p) m (DR.DampedRational (BlockBase b a) v a)
getBlockExprContinuum derivVec xVal =
  evalFreeVectM $ \(rationalFn, block) ->
  let r = R.shift xVal rationalFn
  in DR.multiplyRationalFunction r <$> getBlockContinuum derivVec block

getBlockFreeVectIsolated
  :: (HasBlocks b p m a, IsolatedBlock b deriv a)
  => Vector deriv
  -> FreeVect b a
  -> Compose (Tagged p) m (Vector a)
getBlockFreeVectIsolated derivsV v =
  L.reifyVector derivsV $ \derivs ->
  fmap L.toVector (evalFreeVectM (getBlockIsolated derivs) v)

getBlockFreeVectContinuum
  :: (HasBlocks b p m a, ContinuumBlock b deriv a)
  => Vector deriv
  -> FreeVect b a
  -> Compose (Tagged p) m (DampedRational (BlockBase b a) Vector a)
getBlockFreeVectContinuum derivsV v =
  L.reifyVector derivsV $ \derivs ->
  fmap (DR.mapNumerator L.toVector) (evalFreeVectM (getBlockContinuum derivs) v)

type CrossingMat j n b = Matrix j j `Tensor` V n `Tensor` FreeVect b

getCrossingMatIsolated
  :: (HasBlocks b p m a, Dim n, IsolatedBlock b deriv a)
  => V n (Vector deriv)
  -> CrossingMat j n b a
  -> Compose (Tagged p) m (Matrix j j (Vector a))
getCrossingMatIsolated derivsV =
  traverse (fmap Foldable.msum . getElem) . getCompose
  where
    getElem = sequenceA . mzipWith getBlockFreeVectIsolated derivsV . getCompose

getCrossingMatContinuum
  :: (HasBlocks b p m a, Dim n, ContinuumBlock b deriv a)
  => V n (Vector deriv)
  -> CrossingMat j n b a
  -> Compose (Tagged p) m (DampedRational (BlockBase b a) (Matrix j j `Tensor` Vector) a)
getCrossingMatContinuum derivsV =
  fmap DR.sequence .
  traverse (fmap concatElem . getElem) .
  getCompose
  where
    getElem = sequenceA . mzipWith getBlockFreeVectContinuum derivsV . getCompose
    concatElem = DR.mapNumerator (Foldable.msum . getCompose) . DR.sequence
